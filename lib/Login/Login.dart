import 'dart:convert';
import 'dart:io';
import 'package:camerax/Login/Register.dart';
import 'package:camerax/Login/ResetPass.dart';
import 'package:camerax/TabBarNav/TabBarNav.dart';
import 'package:camerax/utils/app_colors.dart';
import 'package:camerax/utils/methods.dart';
import 'package:camerax/utils/raised_button.dart';
// import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}       
 String 
 url ='http://34.122.179.12/camera/login/';
class _LoginPageState extends State<LoginPage> {
  bool loading = false;
bool passWordVisible =true;
final GlobalKey<FormState>_formkey = GlobalKey<FormState>();

void togglePasswordVisiblity() {
    setState(() {
      passWordVisible = !passWordVisible;
    });
  }
_navigatetoForgetPassword(){
  Navigator.pop(context);
Navigator.push(context, MaterialPageRoute(builder: (context)=>ResetPassword() ));    


}
_navigatetoRegister(){
    Navigator.pop(context);
Navigator.push(context, MaterialPageRoute(builder: (context)=>Register() ));    



}

  String _email;
  String _password;

 
  @override
  Widget build(BuildContext context) {

    Future<void> _login(context) async{
      if(_formkey.currentState.validate()){
        setState(() {
          loading = true;
          
        });
        _formkey.currentState.save();

        try{
          Map data ={
            "username":_email,
            "password":_password

          };
          var response = await http.post("$url",body: data);
          var jsonObject = json.decode(response.body);
        //  var userData =(jsonObject as Map<String, dynamic>)["data"];
         Map<String, dynamic> jsonData= json.decode(response.body);
          var jsonres = jsonData["token"].toString();
          if(response.statusCode == 200){
            print(jsonData["token"]);
          setState(() {
          loading = false;
        });
        //  print(jsonData);
        Navigator.pop(context);
        Navigator.push(context, MaterialPageRoute(builder: (context)=>TabBarNav(jsondata: jsonres,) ));    


          }else{
           Methods.showSnackBar("Username or password is incorrect", context);
            setState(() {
          loading = false;
        });
            
          }
 


        }on SocketException catch(error){
          setState(() {
          loading = false;
        });
            Methods.showSnackBar(error.toString(), context);
          // print(error);

        }catch(e){
          setState(() {
          loading = false;
        });
           Methods.showSnackBar(e.toString(), context);
          // print(e);
        }
      }

    }
     final email = TextFormField(
      
      keyboardType: TextInputType.emailAddress,
      autofocus: false,

      decoration: InputDecoration(
        hintText: 'E-mail Id',
        prefixIcon: Image.asset("assets/mail.ico"),
    contentPadding: EdgeInsets.symmetric(vertical: 20.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),),
        
      ),
      validator: (val){
        if(val.isEmpty){
          return "Please Enter the user Name";
        }else{
          return null;
        }
      },
    //  validator: (val)=> !EmailValidator.validate(val, true)
    //  ? 'Not a Valid Email.'
    //               : null,
      onSaved: (val)=>_email=val,

      
    );

    final password = TextFormField(
      
      
      autofocus: false,
      initialValue: '',
      obscureText: passWordVisible,
      decoration: InputDecoration(
        
        prefixIcon: Image.asset("assets/password.ico"),
        hintText: 'Password',
        suffixIcon: IconButton(icon: Icon(
          passWordVisible? Icons.visibility
          : Icons.visibility_off,
          color: Theme.of(context).primaryColor,
        ),
        onPressed:togglePasswordVisiblity,
        iconSize:25.0,),
        contentPadding: EdgeInsets.symmetric(vertical: 20.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      
      
      
     validator: (val)=>val.length < 4 ? "password is too short..":null,
      onSaved: (val)=>_password=val,

    );
    return Scaffold(

         body: SingleChildScrollView(
            child: Form(
              key: _formkey,
                        child: Container(
          child: Column(
              children: <Widget>[
                Container(
                height: MediaQuery.of(context).size.height/3,
                width: MediaQuery.of(context).size.width,
                // color: Colors.green,
                child: Center(
                  child: Image.asset("assets/Logo.png")
                ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:18.0),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("Sign in",style: TextStyle(color: AppColors.colorBlue,fontWeight: FontWeight.bold, fontSize: 25.0 ),),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left:24.0,right: 24.0,top: 30.0),
                  child: email,
                ),
                Padding(
                  padding: const EdgeInsets.only(left:24.0,right: 24.0,top: 30.0),
                  child: password,
                ),
                Padding(
                  padding: const EdgeInsets.only(top:18.0,right: 50.0),
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: InkWell(
                      onTap: (){
                        _navigatetoForgetPassword();
                      },
                      
                      child: Text("forget password?",style: TextStyle(fontWeight:FontWeight.bold,decoration: TextDecoration.underline,fontSize: 15.0 ),)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:20.0),
                  child: Align(
                    alignment: Alignment.center,
                    child: MyRaisedButton(
                      loading: loading,
                      onPressed: _login,
                      title:"Login".toUpperCase(),
                      buttonColor: AppColors.colorBlue,
                      textColor: AppColors.colorWhite,
                      ),
                  ),
                ),
                 Padding(
                  padding: const EdgeInsets.only(top:18.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Don't have an Account ?"),
                      InkWell(
                        onTap: (){
                          _navigatetoRegister();
                        },
                        child: Text("SignUp",style: TextStyle(color: AppColors.colorBlue, fontWeight: FontWeight.bold),))
                    ],
                  ),
                ),
              ],
          ),
          
        ),
            ),
      ),
    );
  }
}