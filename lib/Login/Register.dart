import 'dart:io';

import 'package:camerax/Login/Login.dart';
import 'package:camerax/utils/app_colors.dart';
import 'package:camerax/utils/methods.dart';
import 'package:camerax/utils/raised_button.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart'as http;
import 'dart:convert';
class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {


String _email;
String _password;
String _firstName;
String _lastName;
String _userName;

  bool loading = false;
  bool isstaff = false;
bool passWordVisible =true;
final GlobalKey<FormState>_formkey = GlobalKey<FormState>();

void togglePasswordVisiblity() {
    setState(() {
      passWordVisible = !passWordVisible;
    });
  }


  @override
  Widget build(BuildContext context) {

final email = TextFormField(
      
      keyboardType: TextInputType.emailAddress,
      autofocus: false,

      decoration: InputDecoration(
        hintText: 'E-mail Id',
        // prefixIcon: Image.asset("assets/mail.ico"),
    contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),),
        
      ),
      // validator: (val){
      //   if(val.isEmpty){
      //     return "Please Enter the user Name";
      //   }else{
      //     return null;
      //   }
      // },
     validator: (val)=> !EmailValidator.validate(val, true)
     ? 'Not a Valid Email.'
                  : null,
      onSaved: (val)=>_email=val,

      
    );

    final password = TextFormField(
      
      
      autofocus: false,
      initialValue: '',
      obscureText: passWordVisible,
      decoration: InputDecoration(
        
        // prefixIcon: Image.asset("assets/password.ico"),
        hintText: 'Password',
        suffixIcon: IconButton(icon: Icon(
          passWordVisible? Icons.visibility
          : Icons.visibility_off,
          color: Theme.of(context).primaryColor,
        ),
        onPressed:togglePasswordVisiblity,
        iconSize:25.0,),
        contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      
      
      
     validator: (val)=>val.length < 4 ? "password is too short..":null,
      onSaved: (val)=>_password=val,

    );
     final firstName = TextFormField(
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'FirstName',
        contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
        border: OutlineInputBorder(
          borderSide: new BorderSide(color: Colors.teal),
          borderRadius: BorderRadius.circular(32.0),
        ),
      ),
      validator: (input) {
        if (input.isEmpty) {
          return 'please enter the First Name';
        }else{
        return null;
        }
      },
      onSaved: (input) => _firstName = input,
    );
    final userName = TextFormField(
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'UserName',
        contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
        border: OutlineInputBorder(
          borderSide: new BorderSide(color: Colors.teal),
          borderRadius: BorderRadius.circular(32.0),
        ),
      ),
      validator: (input) {
        if (input.isEmpty) {
          return 'please enter the UserName';
        }else{
        return null;
        }
      },
      onSaved: (input) => _userName = input,
    );
    final lastName = TextFormField(
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'LastName',
        contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
        border: OutlineInputBorder(
          borderSide: new BorderSide(color: Colors.teal),
          borderRadius: BorderRadius.circular(32.0),
        ),
      ),
      validator: (input) {
        if (input.isEmpty) {
          return 'please enter the LastName';
        }else{
          return null;
        }
      },
      onSaved: (input) => _lastName = input,
    );
void _signUp(context)async{
   String url = 'http://34.122.179.12/camera/usercreate/';
  if(_formkey.currentState.validate()){
        setState(() {
          loading = true;
          
        });
        _formkey.currentState.save();

        try{
          Map data ={
            "email":_email,
           "username":_userName,
            "password":_password,
            "first_name":_firstName,
            "last_name":_lastName,
            "is_staff":isstaff.toString()

          };
          var response = await http.post("$url",body: data);
          var jsonObject = json.decode(response.body);
         var userData =(jsonObject as Map<String, dynamic>)["data"];
         Map<String, dynamic> jsonData= json.decode(response.body);
          var jsonres = jsonData;
          print(jsonObject);
          if(response.statusCode == 201){
             print(jsonres);
          setState(() {
          loading = false;
        });
        //  print(jsonData);
        Navigator.pop(context);
         Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginPage() ));    


          }else{
            print(response.statusCode);
            Methods.showSnackBar("Not Registered successfully", context);

             setState(() {
          loading = false;
        });

          }



        }on SocketException catch(error){
          setState(() {
          loading = false;
        });
            Methods.showSnackBar(error.toString(), context);
          // print(error);

        }catch(error){
          setState(() {
          loading = false;
        });
           Methods.showSnackBar(error.toString(), context);
          // print(e);
        }

}
}
    return Scaffold(
      body: SingleChildScrollView(
            child: Form(
              key: _formkey,
                        child: Container(
          child: Column(
              children: <Widget>[
                Container(
                height: MediaQuery.of(context).size.height/4,
                width: MediaQuery.of(context).size.width,
                // color: Colors.green,
                child: Center(
                  child: Image.asset("assets/Logo.png")
                ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:18.0),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("Sign Up",style: TextStyle(color: AppColors.colorBlue,fontWeight: FontWeight.bold, fontSize: 25.0 ),),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left:24.0,right: 24.0,top: 30.0),
                  child: email,
                ),
                Padding(
                  padding: const EdgeInsets.only(left:24.0,right: 24.0,top: 30.0),
                  child: userName,
                ),
                Padding(
                  padding: const EdgeInsets.only(left:24.0,right: 24.0,top: 30.0),
                  child: firstName,
                ),
                Padding(
                  padding: const EdgeInsets.only(left:24.0,right: 24.0,top: 30.0),
                  child: lastName,
                ),
                Padding(
                  padding: const EdgeInsets.only(left:24.0,right: 24.0,top: 30.0),
                  child: password,
                ),
                 Align(
                   alignment: Alignment.topLeft,
                                    child: Padding(
                    padding: const EdgeInsets.only(left:24.0,top: 30.0),
                    child: Row(
                      children: <Widget>[
                        Checkbox(value: isstaff,
                         onChanged: (bool value){
                           setState(() {
                             isstaff= value;
                           });
                           print(value);
                         }),
                         Text("is Staff")
                      ],
                    ),
                ),
                 ),
              
                Padding(
                  padding: const EdgeInsets.only(top:20.0),
                  child: Align(
                    alignment: Alignment.center,
                    child: MyRaisedButton(
                      loading: loading,
                      onPressed: _signUp,
                      title:"sign up".toUpperCase(),
                      buttonColor: AppColors.colorBlue,
                      textColor: AppColors.colorWhite,
                      ),
                  ),
                )
              ],
          ),
          
        ),
            ),
      ),

      
    );
  }
}