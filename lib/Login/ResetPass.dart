import 'package:camerax/Login/Login.dart';
import 'package:camerax/utils/app_colors.dart';
import 'package:camerax/utils/raised_button.dart';
import 'package:flutter/material.dart';

class ResetPassword extends StatefulWidget {
  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  @override
  Widget build(BuildContext context) {

String _email;
 _reset(context){}

    final email = TextFormField(
      
      keyboardType: TextInputType.emailAddress,
      autofocus: false,

      decoration: InputDecoration(
        hintText: 'E-mail Id',
        prefixIcon: Icon(Icons.email),
    contentPadding: EdgeInsets.symmetric(vertical: 20.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),),
        
      ),
      validator: (input){
        if(input.isEmpty){
        return 'please enter the email';
        }else{
          return null;
        }
      },
      onSaved: (input)=>_email=input,

      
    );
   return Scaffold(
        body: Container(
        child: Column(
          children: <Widget>[
            Container(
            height: MediaQuery.of(context).size.height/3,
            width: MediaQuery.of(context).size.width,
            
          child: Center(
                  child: Image.asset("assets/Logo.png")
                ),
            ),
            Padding(
              padding: const EdgeInsets.only(top:18.0),
              child: Align(
                alignment: Alignment.center,
                child: Text("Reset Your Password",style: TextStyle(color: AppColors.colorBlue,fontWeight: FontWeight.bold, fontSize: 25.0 ),),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left:24.0,right: 24.0,top: 30.0),
              child: email,
            ),
          
           
            Padding(
              padding: const EdgeInsets.only(top:40.0),
              child: Align(
                alignment: Alignment.center,
                child: MyRaisedButton(
                  onPressed: _reset,
                  title:"Reset".toUpperCase(),
                  buttonColor: AppColors.colorBlue,
                  textColor: AppColors.colorWhite,
                  ),
              ),
            ),
             Padding(
              padding: const EdgeInsets.only(top:30.0,),
              child: Align(
                alignment: Alignment.center,
                child: InkWell(
                  onTap: (){
                    Navigator.pop(context);
        Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginPage() ));    

                  },
                  child: Text("Back to Sign in",style: TextStyle(color: AppColors.colorBlue, fontSize: 20.0,decoration: TextDecoration.underline  ),)),
              ),
            ),
          ],
        ),
        
      ),
   );
  }
}