import 'package:camerax/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flushbar/flushbar.dart';

class EditEmployees extends StatefulWidget {
final details;
final String tokens;

EditEmployees({
  this.details,
  this.tokens
});

  @override
  _EditEmployeesState createState() => _EditEmployeesState();
}

class _EditEmployeesState extends State<EditEmployees> {


 
  @override
  Widget build(BuildContext context) {

     _deleteEmployee(empid)async{
try{

     String url ="http://34.122.179.12/camera/employee/$empid";
      var response = await http.delete("$url",headers:{"Authorization":"token ${widget.tokens}"});
   if(response.statusCode==204){
  
    Navigator.pop(context,true); 
 Flushbar(
          message: "Data Deleted Sucessfully",
          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
      
        )..show(context);
        
        
        
   }else{
      Flushbar(
          message: "Data not Deleted Sucessfully",
          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
      // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
     
   }
   
}catch(error){
   Flushbar(
          message:error.toString(),
          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
      // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
   
   
}

  }
    return Scaffold(
      appBar: AppBar(
        elevation: 0.2,
        backgroundColor: AppColors.colorWhite,
        title: Text("Edit Employee Details",style: TextStyle(color:Colors.black),),
        iconTheme: IconThemeData(
          color: Colors.black
          
        ),
      ),
          body: Padding(
        padding: EdgeInsets.only(top:90,left:18.0,right: 18.0,bottom: 18.0),
        child: Stack(
          children: <Widget>[
            Card(
              shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0)
                    ),
              elevation: 0.5,
              child: Container(
                // height: MediaQuery.of(context).size.height,
                child: SingleChildScrollView(
                                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right:30.0,top: 30.0),
                        child: Align(
                          alignment: Alignment.topRight,
                          child: Text(widget.details['name'].toString(),style: TextStyle(fontWeight: FontWeight.bold),),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 25),
                      child: Divider(thickness: 0.5,),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left:15.0,right: 15.0,),
                        child: TextFormField(
                          initialValue: widget.details["name"].toString(),
                          decoration: InputDecoration(
                            labelText: "Name"
                          ),
                        )
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top:5.0),
                        child: Divider(),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left:15.0,right: 15.0,),
                        child: TextFormField(
                          initialValue: widget.details["age"].toString(),
                          decoration: InputDecoration(
                            labelText: "age"
                          ),
                        )
                        
                      ),
                       Padding(
                        padding: const EdgeInsets.only(top:5.0),
                        child: Divider(),
                      ),
                       Padding(
                        padding: const EdgeInsets.only(left:15.0,right: 15.0,),
                        child: TextFormField(
                          initialValue: widget.details["gender"].toString(),
                          decoration: InputDecoration(
                            labelText: "Gender"
                          ),
                        )
                      ),
                       Padding(
                        padding: const EdgeInsets.only(top:5.0),
                        child: Divider(),
                      ),
          
                      Padding(
                        padding: const EdgeInsets.only(left:15.0,right: 15.0,),
                        child: TextFormField(
                          initialValue: widget.details["email"].toString(),
                          decoration: InputDecoration(
                            labelText: "Email Id"
                          ),
                        )
                       
                      ),
                       Padding(
                        padding: const EdgeInsets.only(top:5.0),
                        child: Divider(),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left:15.0,right: 15.0,),
                        child:TextFormField(
                          initialValue: widget.details["contact"].toString(),
                          decoration: InputDecoration(
                            labelText: "Phone Number"
                          ),
                        )
                        
                      ),
                       Padding(
                        padding: const EdgeInsets.only(top:5.0),
                        child: Divider(),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left:15.0,right: 15.0,top: 8.0),
                        child:TextFormField(
                          initialValue: widget.details["address"].toString(),
                          decoration: InputDecoration(
                            labelText: "Address"
                          ),
                        ) 
                        
                      ),
                       Padding(
                        padding: const EdgeInsets.only(top:5.0),
                        child: Divider(),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0,left: 18),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text("Actions",style: TextStyle(fontWeight: FontWeight.bold,color: AppColors.colorGrey,fontSize: 12.0),)),
                      ),
                       Padding(
                        padding: const EdgeInsets.only(top:5.0),
                        child: Divider(),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0,left: 18),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text("Update",style: TextStyle(color: AppColors.liteGreen,fontWeight: FontWeight.bold),)),
                      ),
                      
                     Padding(
                        padding: const EdgeInsets.only(top:5.0),
                        child: Divider(),
                      ),
                       
                    ],
                  ),
                ),
              ),
            ),
            Transform.translate(
              offset: Offset(40, -40),
                        child: CircleAvatar(
                          // backgroundImage: NetworkImage(widget.details["employee_media"].toString()),
                                  
                radius: 50.0,
              ),
            )
          ],
        ),
        
      ),
    );
  }
}
