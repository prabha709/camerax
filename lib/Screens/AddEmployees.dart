import 'package:camerax/utils/app_colors.dart';
import 'package:camerax/utils/methods.dart';
import 'package:camerax/utils/raised_button.dart';
import 'package:email_validator/email_validator.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:io';


class AddEmployees extends StatefulWidget {
  final tokens;

  AddEmployees({this.tokens});

  @override
  _AddEmployeesState createState() => _AddEmployeesState();
}

class _AddEmployeesState extends State<AddEmployees> {
  String _name;
  String _age;
  File _image;
  String _email;
  String _phone;
  String _address;

  bool loading = false;
  int _value = 0;
  String _gender;

  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  
  _pickImage() async {

           var image = await FilePicker.getFile(
      type: FileType.video,
    
      

    );
      int sizeInBytes = image.lengthSync();
      double sizeInMb = sizeInBytes/(1024 *1024);
     
      if(sizeInMb < 10){
        setState(() {
      _image =image;
    });
        
      }else{
        Flushbar(
          message: "Video size should not exceed 10MB",
          duration: Duration(seconds: 2),
          flushbarPosition: FlushbarPosition.TOP,
      // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
        setState(() {
          _image= null;

        });
        
              }
  }

  Future createEmployees(context) async {
    
        String url = "http://34.122.179.12/camera/employee/";
    if (_formkey.currentState.validate()) {
      setState(() {
        loading = true;
      });
      if(_image== null){
        Methods.showSnackBar("Kindly Select the Video", context);
        setState(() {
        loading = false;
      });
      }
      _formkey.currentState.save();
     
      var request = http.MultipartRequest("POST", Uri.parse("$url"));
      request.headers["Authorization"] = "token ${widget.tokens}";
      request.files.add(await http.MultipartFile.fromPath(
        "employee_media", _image.path,));
      request.fields["name"] = _name;
      request.fields["age"] = _age.toString();
      request.fields["email"] = _email;
      request.fields["contact"] = _phone.toString();
      request.fields["address"] = _address;
      request.fields["store"] = 8.toString();
      request.fields["gender"] = _gender;

      request.send().then((result) async {
        http.Response.fromStream(result).then((response) {
          if (response.statusCode == 201) {
            print(response.statusCode);
            setState(() {
              loading = false;
              _image = null;
            });
            _formkey.currentState.reset();
           Methods.showSnackBar("Employee Created Successfully", context);
          }else if(response.statusCode == 413){
               setState(() {
              loading = false;
              // _image = null;
            });
            Methods.showSnackBar("Please upload the video within 1MB", context);
         

          }
          
          else{
            print(response.statusCode);
            setState(() {
              loading = false;
              // _image = null;
            });
            Methods.showSnackBar("Something Went Wrong Please try again later", context);
          }
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0.2,
          backgroundColor: AppColors.colorWhite,
          title: Text(
            "Create Employee",
            style: TextStyle(color: Colors.black),
          ),
          iconTheme: IconThemeData(color: Colors.black),
        ),
        resizeToAvoidBottomInset: true,
        // resizeToAvoidBottomPadding: true,

        body: Padding(
          padding:
              EdgeInsets.only(top: 90, left: 18.0, right: 18.0, bottom: 18.0),
          child: Stack(
            children: <Widget>[
              Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0)),
                elevation: 0.5,
                child: Container(
                  height: MediaQuery.of(context).size.height / 1.4,
                  child: Form(
                    key: _formkey,
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 75),
                            child: Divider(
                              thickness: 0.5,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 8.0),
                            child: Row(
                              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "Name",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Flexible(
                                    child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10.0, right: 10.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 8.0),
                                    child: TextFormField(
                                        // decoration: InputDecoration(
                                        //     border: InputBorder.none),
                                        validator: (value) {
                                          if (value.isEmpty) {
                                            return "This field is required";
                                          } else {
                                            return null;
                                          }
                                        },
                                        onSaved: (value) => _name = value),
                                  ),
                                ))
                              ],
                            ),
                          ),
                          
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 8.0),
                            child: Row(
                              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "Age",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Flexible(
                                    child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10.0, right: 10.0),
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width / 5.7,
                                    height:
                                        MediaQuery.of(context).size.height / 25,
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 8.0, right: 8.0),
                                      child: TextFormField(
                                        keyboardType:TextInputType.phone,
                                          // decoration: InputDecoration(
                                          //     border: InputBorder.none),
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return "Required";
                                            } else {
                                              return null;
                                            }
                                          },
                                          onSaved: (value) => _age = value),
                                    ),
                                  ),
                                ))
                              ],
                            ),
                          ),
                         
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 8.0),
                            child: Row(
                              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "Gender",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: DropdownButton(
                                    value: _value,
                                    items: [
                                      DropdownMenuItem(
                                        child: Text("Select"),
                                        value: 0,
                                      ),
                                      DropdownMenuItem(
                                        child: Text("Male"),
                                        value: 1,
                                      ),
                                      DropdownMenuItem(
                                        child: Text("Female"),
                                        value: 2,
                                      ),
                                    ],
                                    onChanged: (value) {
                                      setState(() {
                                        _value = value;
                                      });
                                      if (_value == 2) {
                                        setState(() {
                                          _gender = "F";
                                          print(_gender);
                                        });
                                      } else {
                                        _gender = "M";
                                        print(_gender);
                                      }
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 8.0),
                            child: Row(
                              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "Email",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Flexible(
                                    child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10.0, right: 10.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 8.0),
                                    child: TextFormField(
                                      keyboardType: TextInputType.emailAddress,
                                        // decoration: InputDecoration(
                                        //     border: InputBorder.none),
                                        validator: (value) => !EmailValidator.validate(value, true)
                  ? 'Not a Valid Email.'
                  : null,
                                        onSaved: (value) => _email = value),
                                  ),
                                ))
                              ],
                            ),
                          ),
                         
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 8.0),
                            child: Row(
                              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "Phone",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Flexible(
                                    child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10.0, right: 10.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 8.0),
                                    child: TextFormField(
                                      keyboardType: TextInputType.phone,
                                        // decoration: InputDecoration(
                                        //     border: InputBorder.none),
                                        validator: (value) => value.length < 10
                  ? "Please enter the valid mobile number"
                  : null,
                                        onSaved: (value) => _phone = value),
                                  ),
                                ))
                              ],
                            ),
                          ),
                          
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 8.0),
                            child: Row(
                              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "Address",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Flexible(
                                    child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10.0, right: 10.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 8.0),
                                    child: TextFormField(
                                        // decoration: InputDecoration(
                                        //     border: InputBorder.none),
                                        validator: (value) {
                                          if (value.isEmpty) {
                                            return "Required";
                                          } else {
                                            return null;
                                          }
                                        },
                                        onSaved: (value) => _address = value),
                                  ),
                                ))
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 88.0, right: 88.0, top: 20),
                            child: MyRaisedButton(
                              loading: loading,
                              onPressed: createEmployees,
                              title: "Create".toUpperCase(),
                              buttonColor: AppColors.liteGreen,
                              textColor: AppColors.colorWhite,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(40, -40),
                child: InkWell(
                  onTap: () {
                    _pickImage();
                  },
                  child: CircleAvatar(
                  child: Center(
                    child: Icon(Icons.videocam,size: 40.0,),
                  ),
                  radius: 50.0,
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(80, 60),
                child: _image == null ?Text("Select the Video"):Text(_image.path.split('/').last,style: TextStyle(color:Colors.red),)
              ),
            ],
          ),
        ));
  }
}
