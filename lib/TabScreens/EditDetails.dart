import 'dart:io';
import 'package:camerax/utils/app_colors.dart';
import 'package:camerax/utils/methods.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart'as http;
import 'dart:convert';

class EditDetails extends StatefulWidget {
final String token;

EditDetails({
  this.token

});

  @override
  _EditDetailsState createState() => _EditDetailsState();
}

class _EditDetailsState extends State<EditDetails> {

   Future getData() async {
    try{
      String url1 ='http://34.122.179.12/camera/store/8';

    // Map headers ={"Authorization":"token ${widget.token}"};
    var response = await http
        .get("$url1", headers: {"Authorization": "token ${widget.token}"});
    var jsonObject = json.decode(response.body);
  if(response.statusCode ==200){
    return jsonObject;

  }else{
    return Methods.showSnackBar("Internal Sever error", context);
  }
  
    }on SocketException catch(error){
      Methods.showSnackBar(error.toString(), context);
    }catch(e){
      Methods.showSnackBar(e.toString(), context);
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: getData(),
        builder: (context, snapshot){
          if(snapshot.data!=null){
            return SingleChildScrollView(
              child: Column(
            children: <Widget>[
              Padding(
        padding: const EdgeInsets.all(18.0),
        child: Card(
          // color: Colors.green,
          elevation: 0.5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0)
          ),
          child: Container(
            // height: MediaQuery.of(context).size.height/3.7,
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                        child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top:20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left:15.0),
                          child: Text("Store Name",style: TextStyle(fontWeight: FontWeight.bold),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right:15.0),
                          child: Text("",style: TextStyle(fontWeight: FontWeight.bold),),
                        )
                      ],
                    ),
                    

                  ),
                  Divider(),
                   Padding(
                    padding: const EdgeInsets.only(top:15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left:15.0),
                          child: Text("Email ID",style: TextStyle(fontWeight: FontWeight.bold),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right:15.0),
                          child: Text("",style: TextStyle(color:AppColors.colorGrey),),
                        )
                      ],
                    ),
                    
                    

                  ),
                  Divider(),
                  Padding(
                    padding: const EdgeInsets.only(top:15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left:15.0),
                          child: Text("Location",style: TextStyle(fontWeight: FontWeight.bold),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right:15.0),
                          child: Text(snapshot.data["location"],style: TextStyle(color:AppColors.colorGrey),),
                        )
                      ],
                    ),
                    
                    

                  ),

                  ExpansionTile(
                    trailing: Icon(Icons.arrow_right),
                    title:Text("Change Password",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15),),
                    children: <Widget>[
                      Text("Click here to Change password")
                      
                    ],
                    ),
                    // Divider(),
                    ExpansionTile(
                    trailing: Icon(Icons.arrow_right),
                    title:Text("Edit",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15),),
                    children: <Widget>[
                      Text("Click here to Edit Details")
                      
                    ],
                    )
                ],
              ),
            ),
            
          ),

        ),
              ),
              Padding(
                padding: const EdgeInsets.only(left:18.0,right: 18.0),
                child: Card(
                  // color: Colors.green,
                  elevation: 0.5,
                  shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0)
          ),
                  child: Container(
                    height: MediaQuery.of(context).size.height/6.5,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left:18.0),
                          child: Text("Active Cameras",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right:30.0),
                          child: Container(
                            width: 80.0,
                            height: 80.0,
                            child: Center(
                              child: Text(snapshot.data["total_camera"].toString(),style: TextStyle(fontSize: 20),),
                            ),
                               decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColors.colorGrey),

                          ),
                        )
                      ],
                    ),

                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left:18.0,right: 18.0),
                child: Card(
                  // color: Colors.green,
                  elevation: 0.5,
                  shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0)
          ),
                  child: Container(
                    height: MediaQuery.of(context).size.height/6.5,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left:18.0),
                          child: Text("Total Cameras",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right:30.0),
                          child: Container(
                            width: 80.0,
                            height: 80.0,
                            child: Center(
                              child: Text(snapshot.data["total_camera"].toString(),style: TextStyle(fontSize: 20),),
                            ),
                               decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColors.colorGrey),

                          ),
                        )
                      ],
                    ),

                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left:18.0,right: 18.0),
                child: Card(
                  // color: Colors.green,
                  elevation: 0.5,
                  shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0)
          ),
                  child: Container(
                    height: MediaQuery.of(context).size.height/6.5,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left:18.0),
                          child: Text("Outer Cameras",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right:30.0),
                          child: Container(
                            width: 80.0,
                            height: 80.0,
                            child: Center(
                              child: Text(snapshot.data["outer_camera_channel_no"].toString(),style: TextStyle(fontSize: 20),),
                            ),
                               decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColors.colorGrey),

                          ),
                        )
                      ],
                    ),

                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left:18.0,right: 18.0),
                child: Card(
                  // color: Colors.green,
                  elevation: 0.5,
                  shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0)
          ),
                  child: Container(
                    height: MediaQuery.of(context).size.height/6.5,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left:18.0),
                          child: Text("Billing Cameras",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right:30.0),
                          child: Container(
                            width: 80.0,
                            height: 80.0,
                            child: Center(
                              child: Text(snapshot.data["billing_camera_channel_no"].toString(),style: TextStyle(fontSize: 20),),
                            ),
                               decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColors.colorGrey),

                          ),
                        )
                      ],
                    ),

                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left:18.0,right: 18.0),
                child: Card(
                  // color: Colors.green,
                  elevation: 0.5,
                  shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0)
          ),
                  child: Container(
                    height: MediaQuery.of(context).size.height/6.5,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left:18.0,),
                          child: Text("Add Employees with \nVideo",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right:30.0),
                          child: Container(
                            width: 80.0,
                            height: 80.0,
                            child: Center(
                              child: Icon(Icons.add,size: 35,),
                            ),
                               decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColors.colorGrey),

                          ),
                        )
                      ],
                    ),

                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left:18.0,right: 18.0),
              child: Card(
                  shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0)
          ),
                elevation: 0.5,
                // color: Colors.green,
                child: Container(
                  // height: MediaQuery.of(context).size.height/7,
                  child: SingleChildScrollView(
                                    child: Column(
                      children: <Widget>[
                        ExpansionTile(
                      trailing: Icon(Icons.arrow_right),
                      title:Text("Report Issues",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15),),
                      children: <Widget>[
                        Text("Click here to Report Issues")
                        
                      ],
                      ),
                      ExpansionTile(
                      trailing: Icon(Icons.arrow_right),
                      title:Text("About",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15),),
                      children: <Widget>[
                        Text("Click here to See About us")
                        
                      ],
                      )

                      ],
                    ),
                  ),
                ),
              ),
              ),
              Padding(padding: EdgeInsets.only(left:18.0,right: 18.0),
              child: Card(
                shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0)
          ),
                color: AppColors.colorGrey,
                child: Container(
                  height: MediaQuery.of(context).size.height/18,
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.power_settings_new),
                        Padding(
                          padding: const EdgeInsets.only(left:8.0),
                          child: Text("LogOut"),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              )

            ],
          ),
      );


          }else if(snapshot.data == null){
            return Center(
              child: SpinKitWave(color: AppColors.colorBlue,),
            );


          }else{
            return Center(child: Text("Edit Your Details Here"),);
            
          }

        },
      )

    );
  }
}