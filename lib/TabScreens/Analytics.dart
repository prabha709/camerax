import 'dart:io';
import 'package:camerax/utils/app_colors.dart';
import 'package:camerax/utils/methods.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
class AnalyticsModelData extends StatelessWidget {
  final String token;

  AnalyticsModelData({this.token});

  @override
  Widget build(BuildContext context) {
    
          return Scaffold(
        body: AnalyticsPage(token: token,),
    );
  }
}

class AnalyticsPage extends StatefulWidget {
  final token;

  AnalyticsPage({this.token});

  @override
  _AnalyticsPageState createState() => _AnalyticsPageState();
}

class _AnalyticsPageState extends State<AnalyticsPage> {
  String selectedDate;
currentDate(){
     final DateTime now = DateTime.now();
   final DateFormat formatter = DateFormat('yyyy-MM-dd');
  final String formatted = formatter.format(now);
  setState(() {
    selectedDate = formatted;
  }); 
}
@override
void initState(){
  super.initState();
  currentDate();
}

  getModeAnalysis() async {
    String url = "http://34.122.179.12/camera/modelanalysis/?dt=$selectedDate";
    try {
      // Map headers ={"Authorization":"token ${widget.token}"};
      var response = await http
          .get("$url", headers: {"Authorization": "token ${widget.token}"});
      var jsonObject = json.decode(response.body);
      if (response.statusCode == 200) {
        // setState(() {
          
        // });
        return jsonObject;
        
      } else {
        Methods.showSnackBar("Internal server Error", context);
        return null;
      }
    } on SocketException catch (error) {
      Methods.showSnackBar(error.toString(), context);
    } catch (e) {
      print(e);
      // Methods.showSnackBar(e.toString(), context);
    }
  }
  Future getData() async {
    try {
      String url1 = 'http://34.122.179.12/camera/analyticdisplay/';

      // Map headers ={"Authorization":"token ${widget.token}"};
      var response = await http
          .get("$url1", headers: {"Authorization": "token ${widget.token}"});
      var jsonObject = json.decode(response.body);
      if (response.statusCode == 200) {
        return jsonObject[0];
      } else {
        Methods.showSnackBar("Internal server Error", context);
      }
    } on SocketException catch (error) {
      Methods.showSnackBar(error.toString(), context);
    } catch (e) {
      print(e);
      // Methods.showSnackBar(e.toString(), context);
    }
  }


  pickDate() async {
    DatePicker.showDatePicker(
      context,
      showTitleActions: true,
      minTime: DateTime(2019, 3, 5),
      maxTime: DateTime(2021, 6, 7),
      theme: DatePickerTheme(
          headerColor: Colors.orange,
          backgroundColor: Colors.blue,
          itemStyle: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),
          doneStyle: TextStyle(color: Colors.white, fontSize: 16)),
      onChanged: (date) {
        print('change $date in time zone ' +
            date.timeZoneOffset.inHours.toString());
      },
      onConfirm: (date) {
        var dates = DateTime.parse(date.toString());
        var formattedDate = "${dates.year}-${dates.month}-${dates.day}";
        print('$formattedDate');
        setState(() {
          selectedDate = formattedDate;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    final double itemWidth = size.width / 2;
    return Scaffold(
      body:selectedDate == null ? Center(
      child:Text("Please Select the Date to show Data")): 
      FutureBuilder(
            future: getModeAnalysis(),
            builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.data == null) {
            return Center(
              child: Text("Data Not Found Please Other Select Date "),
            );
          } else {
          return  GridView.builder(
                  
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: itemWidth / itemHeight),
                itemCount: snapshot.data.length,
                itemBuilder: (context, index) {
                  //  selectedItemValue.add("NONE");
                  
                  return DropDownItems(
                      indexData: snapshot.data[index], tokens: widget.token);
                });

              
            
           
          }
        } else if (snapshot.hasError) {
          return Center(
            child: Text("Internal Server Error"),
          );
        } else {
          return Center(
              child: SpinKitWave(
            color: AppColors.colorBlue,
          ));
        }
            },
          ),
    

       
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.calendar_today),
          onPressed: (){
      pickDate();
          }
          ),
        );

  }
}

class DropDownItems extends StatefulWidget {
  final indexData;
  final tokens;

  DropDownItems({this.indexData, this.tokens});
  @override
  _DropDownItemsState createState() => _DropDownItemsState();
}

class _DropDownItemsState extends State<DropDownItems> {
  String selectedItemValue = "NONE";
  String _classType;
  _updateData(indexData) async {
    String url = "http://34.122.179.12/camera/modelanalysis/${indexData["id"]}";
    try {
      Map data = {
        // "img_url": indexData["img_url"].toString(),
        // "timestamp": indexData["timestamp"].toString(),
        // "store": indexData["store"].toString(),
        "updatedclasstype": _classType.toString(),
        "flag": true.toString()
      };
      var response = await http.patch("$url",
          body: data, headers: {"Authorization": "token ${widget.tokens}"});
      var jsonObject = json.decode(response.body);
      if (response.statusCode == 200) {
         setState(() {
                        headings = selectedItemValue;
                      });
        Methods.showSnackBar("Updation Done", context);
      } else {
       
        return Methods.showSnackBar("Updation Not Done", context);
      }
      return jsonObject;
    } on SocketException catch (error) {
      Methods.showSnackBar(error.toString(), context);
    } catch (e) {
      print(e);
      // Methods.showSnackBar(e.toString(), context);
    }
  }

  String result() {
    if (widget.indexData["flag"] == false) {
      if (widget.indexData["classtype"] == "E") {
        return "Employee";
      } else if (widget.indexData["classtype"] == "C") {
        return "Customer";
      } else {
        return "Other";
      }
    } else {
      if (widget.indexData["updatedclasstype"] == "E") {
        return "Employee";
      } else if (widget.indexData["updatedclasstype"] == "C") {
        return "Customer";
      } else {
        return "Other";
      }
    }
  }

  String headings;
  @override
  void initState() {
    super.initState();
    headings = result();
  }

  
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: Color(0xffc4c4c4),
            )),
        padding: EdgeInsets.all(8),
        margin: EdgeInsets.all(8),
        child: Column(
          children: [
            //Product Image
            Container(
              height: MediaQuery.of(context).size.height / 4,
              width: MediaQuery.of(context).size.width / 2,
              child: Image(
                fit: BoxFit.fill,
                // image: NetworkImage(
                //     "https://images3.alphacoders.com/196/thumb-1920-196149.jpg"),
                image: NetworkImage(widget.indexData["img_url"]),
              ),
            ),

            Expanded(
              child: Column(
                children: [
                  Text(headings),
                  Text(widget.indexData["timestamp"].toString()),
                ],
              ),
            ),

            DropdownButton(
              //  isDense: true,
              icon: Icon(Icons.arrow_downward),
              iconSize: 24,
              elevation: 16,
              style: TextStyle(color: Colors.deepPurple),
              underline: Container(
                height: 2,
                color: Colors.deepPurpleAccent,
              ),
              onChanged: (value) {
                setState(() {
                  selectedItemValue = value;

                  _classType = selectedItemValue[0];
                });
                print(selectedItemValue);
                print(_classType);
              },
              value: selectedItemValue,
              items: _dropDownItem(),
            ),

            FlatButton(
              onPressed: selectedItemValue == "NONE"
                  ? null
                  : () async {
                      var response = await _updateData(widget.indexData);
                      print(response);
                      // setState(() {
                      //   headings = selectedItemValue;
                      // });
                    },
              disabledColor: Colors.blueGrey,
              splashColor: Colors.redAccent,
              color: Theme.of(context).accentColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text(
                'Update',
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }
}

List<DropdownMenuItem<String>> _dropDownItem() {
  List<String> ddl = ["NONE", "Employee", "Customer", "Others"];
  return ddl
      .map((value) => DropdownMenuItem(
            value: value,
            child: Text(value),
          ))
      .toList();
}
