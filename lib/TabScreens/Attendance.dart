import 'package:camerax/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart'as http;
import 'package:flushbar/flushbar.dart';
import 'dart:convert';
class Attendance extends StatefulWidget {

  final token;

  Attendance({
    this.token
  });

  @override
  _AttendanceState createState() => _AttendanceState();
}

class _AttendanceState extends State<Attendance> {
  String selectedDateText;
  String selectedDate;
   currentDate(){
     final DateTime now = DateTime.now();
   final DateFormat formatter = DateFormat.yMMMMd('en_US');
final DateFormat dateFormat = DateFormat('yyyy-MM-dd');

  final String formatted = formatter.format(now);
  final String formattedDate = dateFormat.format(now);

  setState(() {
    selectedDateText = formatted;
    selectedDate = formattedDate;
  }); 
}
  @override
  void initState(){
    super.initState();
    currentDate();
  }
  pickDate() async {
    DatePicker.showDatePicker(
      context,
      showTitleActions: true,
      minTime: DateTime(2019, 3, 5),
      maxTime: DateTime(2021, 6, 7),
      theme: DatePickerTheme(
          headerColor: Colors.orange,
          backgroundColor: Colors.blue,
          itemStyle: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),
          doneStyle: TextStyle(color: Colors.white, fontSize: 16)),
      onChanged: (date) {
        print('change $date in time zone ' +
            date.timeZoneOffset.inHours.toString());
      },
      onConfirm: (date) {
        var dates = DateTime.parse(date.toString());
        var formattedDate = "${dates.year}-${dates.month}-${dates.day}";

        print('$formattedDate');
        setState(() {
          selectedDate = formattedDate;
          selectedDateText = formattedDate;
        });
      },
    );
  }
  getAttendance()async{
    try{

     String url ="http://34.122.179.12/camera/employeeattendence/?dt=$selectedDate";
      var response = await http.get("$url",
      headers:{"Authorization":"token ${widget.token}"});
   if(response.statusCode==200){
     var jsonObject = json.decode(response.body);
  return jsonObject;
        
        
        
   }else{
      Flushbar(
          message: response.body,
          backgroundColor: Colors.red,
          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
      // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
        return null;
     
   }
   
}catch(error){
   Flushbar(
          message:error.toString(),
          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
      // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
        return null;
   
   
}

  }
  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
          body: ListView(
        shrinkWrap: true,
          children: [
      Card(
        elevation: 5.0,
                child: ListTile(
          
          leading: Icon(Icons.today,size: 40.0,),
          title: Text(selectedDateText),
          trailing: InkWell(
            onTap: (){
              pickDate();
            },
            child: Icon(Icons.edit)),
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(18.0),
        child: Card(
          elevation: 2.0,
                child: Container(
                  height: MediaQuery.of(context).size.height / 1.3,
                  width: MediaQuery.of(context).size.width,
                  child: FutureBuilder(
            future: getAttendance(),
            builder: (context, snapshot){
              if(snapshot.connectionState==  ConnectionState.done){
                  if(snapshot.data == null){
                    return Center(child: Text("Data not Proccessed Yet"),);
                  }else{
                    return ListView.builder(
                      shrinkWrap: true,
                      physics: AlwaysScrollableScrollPhysics(),
                     
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index){
                        // print(snapshot.data[index]["attendences"].length==0  ? "null": 
                        // snapshot.data[index]["attendences"][0]["created_at"]);
                        // return Text(snapshot.data[index]["name"]);
                        return Card(
                          elevation: 2.0,
                                                  child: ListTile(
                            leading: CircleAvatar(),
                            title:Text(snapshot.data[index]["name"]),
                            subtitle:Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(snapshot.data[index]["id"].toString()),
                                snapshot.data[index]["attendences"].length==0 ?Container():
                                Text(snapshot.data[index]["attendences"][0]["created_at"])
                                
                              ],
                            ),
                            trailing: snapshot.data[index]["attendences"].length== 0 ? 
                            CircleAvatar(backgroundColor: Colors.red,child: Text("A",style: TextStyle(color: Colors.white),),):
                            CircleAvatar(backgroundColor: Colors.greenAccent,child: Text("P",style: TextStyle(color: Colors.white)),)
                            ,
                          ),
                        );
                      });

                  }
              }else if(snapshot.hasError){
                  return Center(child: Text("Data not Proccessed Yet"),);
              }else{
                  return Center(
                    child: SpinKitWave(color: AppColors.colorBlue,),
                  );
              }
            },
            
          ),
                ),
        ),
      )
        
      
          ],
        ),
    );
  }
}