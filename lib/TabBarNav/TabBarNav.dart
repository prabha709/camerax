import 'package:camerax/TabScreens/Analytics.dart';
import 'package:camerax/TabScreens/Attendance.dart';
import 'package:camerax/TabScreens/EditDetails.dart';
import 'package:camerax/TabScreens/Employees.dart';
import 'package:camerax/TabScreens/Total.dart';
import 'package:camerax/TabScreens/TotalTesting.dart';
import 'package:camerax/utils/app_colors.dart';
import 'package:flutter/material.dart';

class TabBarNav extends StatefulWidget {
final String jsondata;

TabBarNav({
  this.jsondata

});

  @override
  _TabBarNavState createState() => _TabBarNavState();
}

class _TabBarNavState extends State<TabBarNav> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        
        length: 6,
        child: Scaffold(
      
          appBar: new PreferredSize(
            child:Container(
               
              child: new SafeArea(
                child: Column(
                  children: <Widget>[
                    new Expanded(child:new Container()),
                    TabBar(
                      isScrollable: true,
                      // labelColor: Colors.green,
                      indicatorColor: Colors.orange,
                      indicator: 
                      // ShapeDecoration(shape: RoundedRectangleBorder(
                      //   borderRadius: BorderRadius.circular(10.0)
                      // )),
                      
                      UnderlineTabIndicator(
                        
                        borderSide: BorderSide(width: 5,color: AppColors.colorBlue),
                        insets: EdgeInsets.only(right: 10,left: 10)
                      ),
                      tabs:[
                      
                      Padding(
                        padding: const EdgeInsets.only(bottom:8.0),
                        child: Text("Anotation",style: TextStyle(color:AppColors.colorBlack,fontSize: 13),),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom:8.0),
                        child: Text("Analysis",style: TextStyle(color:AppColors.colorBlack,fontSize: 13),),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom:8.0),
                        child: Text("Total",style: TextStyle(color:AppColors.colorBlack,fontSize: 13),),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom:8.0),
                        child: Text("Employees",style: TextStyle(color:AppColors.colorBlack,fontSize: 13),),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom:8.0),
                        child: Text("Attendance",style: TextStyle(color:AppColors.colorBlack,fontSize: 13),),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom:8.0),
                        child: Text("Edit",textAlign: TextAlign.left, style: TextStyle(color:AppColors.colorBlack,fontSize: 13),),
                      )
                    ] )
                  ],
                ),
              ),
            ),
          
          preferredSize:Size.fromHeight(kToolbarHeight) ),
          body: TabBarView(
            children:[
             
              AnalyticsModelData(token:widget.jsondata),
              TestingAnalysisPage(token:widget.jsondata),
              Total(token:widget.jsondata),
              EmployeesPage(token: widget.jsondata,),
              Attendance(token: widget.jsondata,),
              EditDetails(token: widget.jsondata,)


          ]),
        ),
      ),
      
    );
  }
}